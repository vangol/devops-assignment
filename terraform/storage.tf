resource "random_string" "storageaccount" {
  length  = 6
  special = false
  upper   = false
}

resource "azurerm_storage_account" "default" {
  name                     = "clay-devops-test-${random_string.storageaccount.result}"
  resource_group_name      = azurerm_resource_group.resourcegroup.name
  location                 = azurerm_resource_group.resourcegroup.location
  account_tier             = "Standard"
  account_replication_type = "GRS"
}
